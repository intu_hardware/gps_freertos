#define TINY_GSM_MODEM_SIM800
#include <STM32FreeRTOS.h>
#include <task.h>
#include <TinyGsmClient.h>
#include <Wire.h>
#include <TinyGPS.h>
#include <GSMSimSMS.h>
#include "STM32LowPower.h"

xTaskHandle xGPSHandle = NULL;
xTaskHandle xSMSHandle = NULL ;
xTaskHandle xLBSHandle = NULL;
xTaskHandle xTCPHandle = NULL;

String data, raw, data_up, data_send, interm;
int indx, ind, y;
char Serial3Buffer[600];
int i = 0;
int LBS;
String voltage;
String charge;
int flag = 0;
String ID, iID;
long lt, ln;
String lsb = "0";
float lt_val, ln_val;
String latitude, longitude, batt1, apn, pingy, clk, batt, Md, ip, port, heartbeat = "3";
TinyGPS gps;
char *mem1 = "SM";
char *mem2 = "ME";
char *mem3 = "SM_P";
int z;
char *num = "+917337863836";
void GPSTask( void *pvParameters );
void SMSTask( void *pvParameters );
void LBSTask( void *pvParameters );
void TCPTask( void *pvParameters );

HardwareSerial Serial3(USART3);
HardwareSerial Serial2(USART2);
GSMSimSMS sms(Serial3);
#define SerialAT Serial3
TinyGsm modem(SerialAT);
TinyGsmClient client(modem);

void SMSIntrrupt(){
  Serial.println("ISR");
  BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  xTaskNotifyFromISR(xSMSHandle,0,eNoAction,&xHigherPriorityTaskWoken);
}
void setup(){
  flag=0;
  Serial.begin(9600);
  Serial3.begi
  LowPower.begin();
  Serial.println("Setup");
  Serial3.print("AT+CMGF=1");
  Serial3.print("AT+CFGRI=1");
  rtos_delay(200);
  rtos_delay(1000);
  Serial3.print("AT+CNMI=2,2,0,0");
  rtos_delay(1000);
  printSerialData();
  pinMode(PA0, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(PA0), SMSinterrupt, FALLING);
  pinMode(PA6, INPUT);
  sendCmd("AT+CPMS?");
  rtos_delay(1000);
  printSerialData();
  sms.init(); 
  rtos_delay(1000);
  Serial3.println(sms.setPreferredSMSStorage(mem1, mem2, mem3));
  sms.deleteAll();
  flag = false;

  sendCmd("AT+CGSN\r\n");
  rtos_delay(100);
  i = readBufferUntil(Serial3Buffer, 100, "OK\r\n", 5);
  Serial.println("Re-connecting...");
  Serial.println(Serial3Buffer);
  Serial.println("Re-connecting...");
  iID = Serial3Buffer;
  int on = iID.indexOf("O");
  ID = iID.substring(on - 19, on - 5);
  Serial.println(ID);
  rtos_delay(50);
  cleanBuffer(Serial3Buffer, 600);
  rtos_delay(50);
  modem.sendSMS(num, "[" + ID + "]" + "Reset Complete");
  
                        xTaskCreate(
                          GPSTask
                          ,  (const portCHAR *)"GPS" 
                          ,  2560 
                          ,  NULL
                          ,  3
                          ,  &xGPSHandle );
                        xTaskCreate(
                           LBSTask
                           ,  (const portCHAR *)"LBS"
                           ,  2560
                           ,  NULL
                           ,  1
                           ,  &xLBSHandle);
                        xTaskCreate(
                          SMSTask
                          ,  (const portCHAR *) "SMS"
                          ,  1024
                          ,  NULL  
                          ,  1  
                           ,  &xSMSHandle );
                        xTaskCreate(
                          TCPTask
                          ,  (const portCHAR *) "TCP"
                          ,  1024
                          ,  NULL  
                          ,  1  
                          ,  &xTCPHandle );

  vTaskStartScheduler();
  Serial.println("Insufficient RAM");
  while(1);
}

void loop()
{
  
}

void GPSTask( void *pvParameters __attribute__((unused)) )  
{

  for (;;)
  {
      if(lt_val != 0.00){

      sendCmd("AT+CBC=?\r\n");
      i = readBufferUntil(Serial3Buffer, 40, "OK\r\n", 5);
      Serial.println("Re-connecting...");
      Serial.println(Serial3Buffer);
      Serial.println("Re-connecting...");
      cleanBuffer(Serial3Buffer, 40);

      sendCmd("AT+CBC\r\n");
      i = readBufferUntil(Serial3Buffer, 100, "OK\r\n", 5);
      Serial.println("Re-connecting...");
      Serial.println(Serial3Buffer);
      Serial.println("Re-connecting...");
      batt1 = Serial3Buffer;
      Serial.println(batt1);
      int vi = batt1.indexOf(',');
      if(batt.charAt(vi+3)==0){
      batt1 = batt1.substring(vi+1, vi+3);
      }
      else
      {
        batt1 = batt1.substring(vi+1, vi+2);
       }
      Serial.println("/////////data/////////");
      Serial.println("batt1");
      rtos_delay(50);
      Loc=batt1;
      sendCmd("AT+CCLK?\r\n");
      i = readBufferUntil(Serial3Buffer, 100, "OK\r\n", 5);
      Serial.println(Serial3Buffer);
      clk = Serial3Buffer;
      Serial.println(clk);
      int q = clk.indexOf('"');
      clk = clk.substring(q+1, q+18);
      Serial.println(clk);
      Serial.println(batt);
      Serial.println(voltage);
      Serial.println(clk);
      rtos_delay(50);
      Loc+=";"+ clk;
      getGPS();

          Serial.println(lt_val);
          Serial.println(ln_val);
          Serial.print("Latitude : ");
          Serial.print(lt_val);
          Serial.print(" :: Longitude : ");
          Serial.println(ln_val);
          latitude = convertFloatToString(lt_val);
          longitude = convertFloatToString(ln_val);
          Loc+= ";"+ latitude + ',' + longitude;
          lt_val = NULL;
          ln_val = NULL;
          Serial.println(Loc);
          Serial.println(lt_val);
          Serial.println(ln_val);
          TCP();
        }

        else{
          LBS();
          TCP();
        }

        LowPower.shutdown(10000);
      
}
}
void SMSTask( void *pvParameters __attribute__((unused)) )  // This is a Task.
{
  String Md;
  for (;;)
  {
    if(xTaskNotifyWait(0,0,NULL,portMAX_rtos_delay) == pdPASS)
    {  
       Md = "";

    sms.send(num, "OK");
    unsigned int i = 1;
    String msg = sms.read(1);

    sms.deleteAll();
    sms.deleteOne(i);
    Serial3.print("AT+CMGD=ALL");
    rtos_delay(2000);
    Serial.println(msg);
    flag = 0;

    if (msg.indexOf("status") > -1)
    {

      sendCmd("AT+CBC=?\r\n");
      i = readBufferUntil(Serial3Buffer, 40, "OK\r\n", 5);
      Serial.println("Re-connecting...");
      Serial.println(Serial3Buffer);
      Serial.println("Re-connecting...");
      cleanBuffer(Serial3Buffer, 40);

      sendCmd("AT+CBC\r\n");
      i = readBufferUntil(Serial3Buffer, 100, "OK\r\n", 5);
      Serial.println("Re-connecting...");
      Serial.println(Serial3Buffer);
      Serial.println("Re-connecting...");
      batt1 = Serial3Buffer;
      Serial.println(batt1);
      int vi = batt1.indexOf(',');
      if(batt.charAt(vi+3)==0){
      batt1 = batt1.substring(vi+1, vi+3);
      }
      else
      {
        batt1 = batt1.substring(vi+1, vi+2);
       }
      Serial.println("/////////data/////////");
      Serial.println("batt1");
      rtos_delay(50);
      Md = ("ID: " + ID + ",Battery: " + batt1 + "%," + "Mode: ");
      flag = 0;
      cleanBuffer(Serial3Buffer, 100);
      sms.deleteAll();
    }
    if (msg.indexOf("reset") > -1)
    {
      //reset functions
      Serial.println("inside");
      rtos_delay(1000);
      LowPower.shutdown(1000); //reset command
      rtos_delay(1000);
      
      Md = "Device" + ID + "Reset Sucessfully";
      sms.send(num, Md);
      Serial.println("inside");
      flag = 0;
      sms.deleteAll();
      Serial.println("inside");
    }
    if (msg.indexOf("GE:pi") > -1)
    { 

      pingy = msg.substring(95, 97);
      Md = "Ping set to " + pingy + "mins" + " successfully";
      int pingy_x = pingy.toInt();
      int pingy_inter = pingy_x * 60;
      d = d + pingy_inter;
      t1 = t1 + pingy_inter;
      t2 = t2 + pingy_inter;
      flag = 0;
      sms.deleteAll();

    }
    if (msg.indexOf("factory") > -1)
    {
      pingy = 1;
      apn = "Airteliot.com";
      port = "8440";
      ip = "13.127.218.124";
      Md = "Device set to default settings | ";
      Md += "ID: " + ID + "," + " Ping: " + pingy + "mins" + "," + "Ip: " + ip + "," + "Port: " + port + "|";

      flag = 0;
      sms.deleteAll();
    }
    if (msg.indexOf("apn") > -1)
    {

      apn = msg.substring(94, msg.indexOf('#'));
      Serial.println(apn);
      Md = "APN set to " + apn + " sucessfully";
      flag = 0;
      sms.deleteAll();
    }
    if (msg.indexOf("server") > -1)
    {

      Serial3.println("AT+CENG=0\r\n");
      rtos_delay(200);
      printSerialData();

      rtos_delay(5000);
      Serial3.println("AT+CIPCLOSE");
      printSerialData();

      Serial3.println("AT+CIPSHUT");
      rtos_delay(1000);
      printSerialData();
      Serial3.println("AT+CIPMUX=0");
      rtos_delay(2000);
      printSerialData();

      Serial3.println("AT+CGATT=1");
      rtos_delay(100);
      printSerialData();
      Serial3.println("AT+CGATT?");
      rtos_delay(1000);
      printSerialData();

      
      Serial3.println("AT+CSTT=\"Airteliot.com\",\"\",\"\"");
      rtos_delay(5000);
      printSerialData();

      Serial3.println("AT+CIICR");
      rtos_delay(6000);
      printSerialData();

      Serial3.println("AT+CIFSR");
      rtos_delay(2000);
      printSerialData();
      //Serial.println(Loc);
      rtos_delay(2000);

      ip = msg.substring(97, msg.indexOf('#'));
      //Serial.println(ip);
      String server;
      Serial3.println("AT+CIPSTART=\"TCP\",\"" + ip + "\",\"8440\"");

      rtos_delay(5000);
      printSerialData();
      rtos_delay(5000);
      Md = "IP set to " + ip + " sucessfully";
      flag = 0;
      sms.deleteAll();
    }
    if (msg.indexOf("port") > -1)
    {

      port = msg.substring(95, msg.indexOf('#'));
      Serial.println(port);
      Md = "Port set to " + port + " sucessfully";
      flag = 0;
      sms.deleteAll();
    }
    if (msg.indexOf("lsb") > -1)
    {

      if (msg.substring(93) == "?")
      {
        if (lsb == "1")

          Md = "Gsm mode is Active";
        else
        {
          Md = "Gsm mode is Inactive";
        }
      }
      else
      {
        lsb = msg.substring(94, msg.indexOf('#'));
        Serial.println(lsb);
        if (lsb == "1")
        {
          LBS = 1;

          Md = "GSM Location Activated";
        }
        else
        {
          LBS = 0;

          Md = "GSM Location De-activated";
        }
      }
      flag = 0;
      sms.deleteAll();
    }
    if (msg.indexOf("heartbeat") > -1)
    {

      String hb;
      hb = msg.substring(100, msg.indexOf('#'));
      Serial.println(hb);
      flag = 0;
      sms.deleteAll();
    }

    sms.send(num, Md);

    }  
    
     
  }
   
     
}
void LBS()
{

     sendCmd("AT+CENG=0\r\n");
     i = readBufferUntil(Serial3Buffer, 32, "OK\r\n", 3);    
     Serial.println("Re-connecting...");
     Serial.println(Serial3Buffer);
     Serial.println("Re-connecting...");
     cleanBuffer(Serial3Buffer, 32);
      sendCmd("AT+CPIN?\r\n");
      i = readBufferUntil(Serial3Buffer, 32, "OK\r\n", 3);
      Serial.println("Re-connecting...");
      Serial.println(Serial3Buffer);
      Serial.println("Re-connecting...");
      cleanBuffer(Serial3Buffer, 32);
      Serial3.println(F("AT+CNETSCAN=0\r\n"));
      rtos_delay(7000);
      printSerialData();
      Serial3.println(F("AT+CNETSCAN=1\r\n"));
      rtos_delay(5000);
      printSerialData();
      Serial3.println(F("AT+CNETSCAN\r\n"));
      readBufferUntil(Serial3Buffer, 599, "OK\r\n", 40);
      data = Serial3Buffer;
      Serial.println(data);
      rtos_delay(3000);

      for (int k = 0; k < 4; k++)
      {
        if (data.indexOf('H') != 0)
        {
          y = data.indexOf('H');
          data.remove(y - 11, 87);
          Serial.println(data);
        }
      }

      for (int x = 16, u = 0; x < 600; x = 16 + 82 * u, u++)
      {
        Serial.println(data);
        Serial.println(x);
        raw = data.substring(x);
        Serial.println(raw);
        ind = raw.indexOf('O');
        Serial.println(ind);
        String data_up = raw.substring(0, ind - 1);
        indx = data_up.indexOf('M');
        Serial.println(indx);
        if (data_up.indexOf("4") > -1)
        {
          data_send += data_up.substring(indx + 4, indx + 7) + "," + data_up.substring(indx + 12, indx + 14) + "," + data_up.substring(indx + 21, indx + 23) + "," + data_up.substring(indx + 31, indx + 35) + "," + data_up.substring(indx + 42, indx + 44) + "," + data_up.substring(indx + 49, indx + 53) + "|";
          Serial.println(data_send);
        }
        Serial.println(u);
      }
      Loc+= ";" + data_send;
      Serial.println(Loc);
      rtos_delay(10000);
    }

    else if (lsb == "0")
    {
      Loc = "HeartBeat";
    }
  }
   

     

void printSerialData()
{
  while (Serial3.available() != 0)
    Serial.write(Serial3.read());
}

void TCP()
{

      Serial3.println("AT+CENG=0\r\n");
      rtos_delay(200);
      printSerialData();
      rtos_delay(100);
      Serial3.println("AT+CIPSHUT");
      rtos_delay(1000);
      printSerialData();
      Serial3.println("AT+CIPMUX=0");
      rtos_delay(2000);
      printSerialData();
      Serial3.println("AT+CGATT=1");
      rtos_delay(100);
      printSerialData();
      Serial3.println("AT+CGATT?");
      rtos_delay(1000);
      printSerialData(); 
      Serial3.println("AT+CSTT=\"Airteliot.com\",\"\",\"\"");
      rtos_delay(5000);
      printSerialData();
      Serial3.println("AT+CIICR");
      rtos_delay(6000);
      printSerialData();
      Serial3.println("AT+CIFSR");
      rtos_delay(2000); 
      printSerialData();
      Serial.println(Loc);
      rtos_delay(2000);
      Serial3.println("AT+CIPSTART=\"TCP\",\"13.127.218.124\",\"8440\"");
      rtos_delay(5000);
      printSerialData();
      rtos_delay(5000);
      Serial3.println("AT+CIPSEND");  
      rtos_delay(5000);
      printSerialData();
      rtos_delay(5000);

                  Serial3.print(ID);
                  Serial3.print(";");
                  Serial3.print(clk);
                  Serial3.print(";");
                  Serial3.print(batt);
                  Serial3.print(";");
                  Serial3.print(voltage);
                  Serial3.print(";");
                  Serial3.print(charge);
                  Serial3.print(";");
                  Serial3.print(Loc);
                  rtos_delay(3000);
                  printSerialData();
                  Serial3.write(0x1A);
                  rtos_delay(3000);
                  printSerialData();
              
      rtos_delay(5000);
      Serial3.println("AT+CIPCLOSE");
      printSerialData();
      Serial3.println("AT+CIPSHUT");
      rtos_delay(1000);
      printSerialData();
      cleanBuffer(Serial3Buffer, 600);
  }

void cleanBuffer(char *buffer, int count)
{
  for (int i = 0; i < count; i++)
  {
    buffer[i] = '\0';
  }
}

String convertFloatToString(float location)
{
  char locn[10];
  String locAsString;

  dtostrf(location, 1, 2, locn);

  locAsString = String(locn);

  return locAsString;
}


void sendCmd(const char *cmd)
{
  Serial3.write(cmd);
}
int readBufferUntil(char *buffer, int count, const char *resp, unsigned int timeOut)
{
  int len = strlen(resp);
  int sum = 0;
  char c = '0';
  int i = 0;
  unsigned long timerStart, timerEnd;
  timerStart = millis();
  while (1)
  {
    while (Serial3.available())
    {
      c = Serial3.read();
      Serial3.print(c);
      sum = (c == resp[sum]) ? sum + 1 : 0;
      if (sum == len)
        break;
      if (c == '\r' || c == '\n')
        c = '$';
      buffer[i++] = c;
      if (i > count - 1)
        break;
    }
    if (i > count - 1)
      break;
    timerEnd = millis();
    if (timerEnd - timerStart > 1000 * timeOut)
    {
      break;
    }
  }
  rtos_delay(500);
  while (Serial3.available())
  { 
    Serial3.read();
  }
  return 0;
}
void getGPS()
{

  bool newdata = false;
  unsigned long start = millis();
  while (millis() - start < 1000)
  {
    if (feedgps())
    {
      newdata = true;
    }
  }

  if (newdata)
  {
    gpsdump(gps);
  }
}
bool feedgps()
{
  while (Serial2.available())
  {
    if (gps.encode(Serial2.read()))
      return true;
  }
  return 0;
}

void gpsdump(TinyGPS &gps)
{
  gps.get_position(&lt, &ln);
  lt_val = lt;
  ln_val = ln;
  {
    feedgps(); 
  }
}
void rtos_delay(uint32_t delay_in_ms)
{
  uint32_t tick_count_local=0;
  //converting ms to ticks
  uint32_t delay_in_ticks = (delay_in_ms  * configTICK_RATE_HZ ) / 1000;
  tick_count_local = xTaskGetTickCount();
  while ( xTaskGetTickCount() < (tick_count_local+delay_in_ticks ));
}